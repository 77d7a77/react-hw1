import './App.css';
import React, {Component} from "react"
import Button from './components/button/button';
import Modal from './components/modal/modal';
class App extends Component{
  constructor(props){
    super(props)
    this.state={
      modal:null
    }
  }
  handleClickFirst=()=>{
    const headerText = "Do you want delete this file";
    const mainText = "Once you delete this file,it won't be possible to undo this action."
    const secondText = "Are you sure you want to delete it?"
    const modal=(<Modal headerTxt={headerText} mainTxt = {mainText} secondTxt = {secondText} closeModal={this.closeModal}/>)
     this.setState({modal:modal});
  }
  handleClickSecond=()=>{
    const headerText2 = "Do you save this file";
    const mainText2 = "Ok,watch for this file again"
    const secondText2 = "Are you sure you want to save it?"
    const modal=(<Modal headerTxt={headerText2} mainTxt = {mainText2} secondTxt = {secondText2} closeModal={this.closeModal}/>)
     this.setState({modal:modal});
  }
  closeModal=()=>{
    this.setState({modal:null})
  }
render(){
     return(
       <>
       <Button color="white" backgroundColor="purple" text = "Open first modal" onClick={this.handleClickFirst} />
       <Button color="white" backgroundColor="black" text = "Open second modal" onClick={this.handleClickSecond} />
       {this.state.modal} 
       </>
     )
   }
  }
export default App
