import React,{Component} from "react";
class Button extends Component{
    constructor(props){
        super(props)
        this.state={
        backgroundColor:props.backgroundColor,
        color:props.color,
        text:props.text,
        border:props.border,
        handleClick:props.onClick,
        className:props.className
        }
    }
   
  render(){
      const style = {color:this.state.color,backgroundColor:this.state.backgroundColor,border:this.state.border}
      return (
        <button style={style} onClick={this.state.handleClick} className={this.state.className}>{this.state.text}</button>
      )
  }
}
export default Button