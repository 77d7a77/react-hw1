import React,{Component} from "react";
import Button from "../button/button";
import './modal.css'
class Modal extends Component{
    constructor(props){
        super(props)
        this.state={
           header:props.headerTxt,
           firsttext:props.mainTxt,
           secondtext:props.secondTxt,
           closeModal:props.closeModal
        }
    }
    
    render(){
        const{header,firsttext,secondtext,closeModal}=this.state
        return(
            <div className="modal-container" onClick={closeModal}>
            <div className="modal" onClick={(e)=>{e.stopPropagation()}}>
            <div className="modal-header">
                <h2 className="header-text" >{header}</h2><Button color="black" backgroundColor="brown" border={0} text="X" onClick={closeModal}/></div>
               <div className="modal-info-text">
                   <p className="modal-text">{firsttext}</p>
                   <p className="modal-text">{secondtext}</p>
                <div className="modal-btns">
                    <Button className="okBtn"border={0} color="white" backgroundColor="brown" text = "Ok"  onClick={closeModal}/>
                    <Button border={0} color="white" backgroundColor="brown"text="Cancel" onClick={closeModal}/>
                </div>
            </div>
            </div>
            </div> 
        )
    }
    
}


export default Modal